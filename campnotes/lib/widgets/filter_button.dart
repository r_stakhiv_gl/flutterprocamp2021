import 'package:flutter/material.dart';

class FilterButton extends StatelessWidget {
  Widget build(BuildContext context) {
    return PopupMenuButton<String>(
      key: const Key('__filterButton__'),
      itemBuilder: (context) => <PopupMenuItem<String>>[
        PopupMenuItem<String>(
          value: 'Show All',
          child: Text('Show All'),
        ),
        PopupMenuItem<String>(
          value: 'Show Active',
          child: Text('Show Active'),
        ),
        PopupMenuItem<String>(
          value: 'Show Completed',
          child: Text('Show Completed'),
        ),
      ],
      icon: Icon(
        Icons.filter_list,
      ),
    );
  }
}

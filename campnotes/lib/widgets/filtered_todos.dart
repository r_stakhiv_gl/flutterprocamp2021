import 'package:campnotes/data/models/todo.dart';
import 'package:campnotes/widgets/delete_todo_snack_bar.dart';
import 'package:campnotes/widgets/todo_item.dart';
import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/widgets.dart';

class FilteredTodos extends StatelessWidget {
  FilteredTodos({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    const itemCount = 3;

    return ListView.builder(
      itemCount: itemCount,
      itemBuilder: (BuildContext context, int index) {
        final todo = Todo(
          'Mock Task',
          complete: index % 2 == 0,
          note: 'Note',
          id: index.toString(),
        );

        return TodoItem(
          todo: todo,
          onDismissed: (direction) {
            ScaffoldMessenger.of(context).showSnackBar(DeleteTodoSnackBar(
              todo: todo,
              onUndo: () {},
            ));
          },
          onTap: () async {},
          onCheckboxChanged: (_) {},
        );
      },
    );
  }
}

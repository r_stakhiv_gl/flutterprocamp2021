import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/widgets.dart';

class ExtraActions extends StatelessWidget {
  ExtraActions({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return PopupMenuButton<String>(
      itemBuilder: (context) => <PopupMenuItem<String>>[
        PopupMenuItem<String>(
          child: Text('Clear Completed'),
          value: 'Clear Completed',
        ),
      ],
    );
  }
}

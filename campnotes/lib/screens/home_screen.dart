import 'package:campnotes/bloc/models/app_tab.dart';
import 'package:campnotes/screens/add_edit_screen.dart';
import 'package:campnotes/widgets/extra_actions.dart';
import 'package:campnotes/widgets/filter_button.dart';
import 'package:campnotes/widgets/filtered_todos.dart';
import 'package:campnotes/widgets/tab_selector.dart';
import 'package:flutter/material.dart';

class HomeScreen extends StatelessWidget {
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Flutter Todo'),
        actions: [
          FilterButton(),
          ExtraActions(),
        ],
      ),
      body: FilteredTodos(),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.of(context).push(
            MaterialPageRoute(
              builder: (context) {
                return AddEditScreen(
                  onSave: (task, note) {},
                  isEditing: false,
                );
              },
            ),
          );
        },
        child: Icon(Icons.add),
        tooltip: 'Add Todo',
      ),
      bottomNavigationBar: TabSelector(
        activeTab: AppTab.todos,
        onTabSelected: (appTab) {},
      ),
    );
  }
}

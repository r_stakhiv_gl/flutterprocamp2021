import 'package:campnotes/screens/home_screen.dart';
import 'package:flutter/material.dart';

class CampnotesApp extends StatelessWidget {
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData.dark().copyWith(
        primaryColor: Colors.grey[800],
        accentColor: Colors.cyan[300],
        buttonColor: Colors.grey[800],
        backgroundColor: Colors.grey[800],
        toggleableActiveColor: Colors.cyan[300],
      ),
      home: HomeScreen(),
    );
  }
}
